package pl.agh.edu;

import pl.agh.edu.dao.UsersDao;
import pl.agh.edu.exceptions.NeedAdminAccessException;
import pl.agh.edu.model.User;
import pl.agh.edu.util.Crypt;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.security.Principal;

@Stateless
public class ChangePasswordService {

    @Resource
    private SessionContext ctx;

    @Inject
    private UsersDao usersDao;

    public void changePassword(Integer id, String password){

        Principal callerPrincipal = ctx.getCallerPrincipal();
        User byId = usersDao.findById(id);
        if (!ctx.isCallerInRole("admin") && !callerPrincipal.getName().equals(byId.getEmail())) {
            throw new NeedAdminAccessException();
        }

        String passwordHash = Crypt.toSHA256(password);

        byId.setPassword(passwordHash);
        usersDao.update(byId);
    }
}
