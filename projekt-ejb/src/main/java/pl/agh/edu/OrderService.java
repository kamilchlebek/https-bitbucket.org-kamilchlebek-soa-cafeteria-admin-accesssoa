package pl.agh.edu;

import pl.agh.edu.dao.OrdersDao;
import pl.agh.edu.dao.SubscriptionDao;
import pl.agh.edu.dao.UsersDao;
import pl.agh.edu.exceptions.NeedAdminAccessException;
import pl.agh.edu.model.MenuItem;
import pl.agh.edu.model.User;
import pl.agh.edu.model.order.Order;
import pl.agh.edu.model.order.OrderStatus;
import pl.agh.edu.model.subscription.Subscription;
import pl.agh.edu.model.subscription.SubscriptionItem;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@RolesAllowed({"admin", "customer", "staff", "menu"})
public class OrderService {

    @Resource
    private SessionContext ctx;

    @Inject
    private OrdersDao ordersDao;
    @Inject
    private SubscriptionDao subscriptionDao;

    @Inject
    private UsersDao usersDao;

    @Inject
    private UserNotificationService userNotificationService;

    @Inject
    private Principal userPrincipal;

    @Inject
    private DeliverService deliverService;

    @Inject
    private PayService payService;

    public void update(Order order, Boolean hasSubscription, Subscription subscription) {

        User byEmail = usersDao.findByEmail(userPrincipal.getName());
        if(hasSubscription){
            List<SubscriptionItem> items = order.getMenuItems().stream().map(SubscriptionItem::copyWithoutId).collect(Collectors.toList());
            subscription.getMenuItems().clear();
            subscription.getMenuItems().addAll(items);
            subscription.setUser(byEmail);
            subscription.setAddress(order.getAddress());

            subscription.setDate(order.getDate());
            subscriptionDao.update(subscription);

        }

        order.setUser(byEmail);
        ordersDao.update(order);

    }

    public void delete(Order order) {

        Principal callerPrincipal = ctx.getCallerPrincipal();
        if (!ctx.isCallerInRole("admin") && !callerPrincipal.getName().equals(order.getUser().getEmail())) {
            throw new NeedAdminAccessException();
        }

        order.setDeleted(true);
        ordersDao.update(order);

    }

    @RolesAllowed({"staff"})
    public void makeComment(Order order) {
        ordersDao.update(order);
    }

    public void handleItemRemove(MenuItem menuItem) {

        List<Order> byMenuItem = ordersDao.findByMenuItem(menuItem);
        byMenuItem.forEach(this::notifyAndDelete);

    }

    private void notifyAndDelete(Order order) {
        if(order.getUser() != null){
            userNotificationService.notifyUser(order.getUser(), "Zamowienie anulowane, zamow jeszcze raz");
        }
        order.setDeleted(true);
        ordersDao.update(order);
    }

    @RolesAllowed({"staff"})
    public void makeDish(Order order) {

        if(order.getAddress() == null || order.getAddress().length() == 0){
            order.setOrderStatus(OrderStatus.WAITING_FOR_PICKUP);
        }
        else {
            order.setOrderStatus(OrderStatus.DONE);
        }

        payService.pay(order);
        ordersDao.update(order);

    }

    @RolesAllowed({"staff"})
    public void toDeliver(Order order) {
        deliverService.toDeliver(order);
        order.setOrderStatus(OrderStatus.BEING_DELIVERED);
        ordersDao.update(order);
    }

    @RolesAllowed({"staff"})
    public void confirmPickedUp(Order order) {
        order.setOrderStatus(OrderStatus.DELIVERED);
        ordersDao.update(order);
    }
}
