package pl.agh.edu;

import pl.agh.edu.dao.MenusDao;
import pl.agh.edu.model.Menu;
import pl.agh.edu.model.MenuCategory;
import pl.agh.edu.model.MenuItem;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;

@Stateless
@RolesAllowed("menu")
public class MenusService {

    @Inject
    private MenusDao menusDao;

    @Inject
    private OrderService orderService;

    @Inject
    private SubscriptionService subscriptionService;

    public void create(Menu menu) {
        menusDao.create(menu);
    }

    public void update(Menu menu) {

        menusDao.update(menu);

    }

    public void deleteMenu(Menu menu) {

        menu.setDeleted(true);
        menusDao.update(menu);

    }

    public void deleteCategory(MenuCategory category) {

        category.setDeleted(true);
        menusDao.update(category.getMenu());

    }

    public void deleteItem(MenuItem menuItem) {

        subscriptionService.handleItemRemove(menuItem);
        orderService.handleItemRemove(menuItem);

        menuItem.setDeleted(true);
        menusDao.update(menuItem.getCategory().getMenu());
    }

    public void saveMenuItem(MenuItem menuItem, MenuCategory sourceCategory) {

        if(!menuItem.getCategory().equals(sourceCategory)){

            if(sourceCategory != null){
                sourceCategory.getItems().remove(menuItem);
            }

            if(menuItem.getCategory().getItems() == null){
                menuItem.getCategory().setItems(new ArrayList<>());
            }
            menuItem.getCategory().getItems().add(menuItem);
        }

        menusDao.update(menuItem.getCategory().getMenu());

    }
}
