package pl.agh.edu.model;


import java.io.Serializable;

public class DeliverRequest implements Serializable  {

    private Integer id;
    private String email;
    private String address;

    public DeliverRequest(Integer id, String email, String address) {
        this.id = id;
        this.email = email;
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
