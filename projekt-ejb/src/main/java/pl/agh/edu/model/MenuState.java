package pl.agh.edu.model;

public enum MenuState {

    ARCHIVED,
    PUBLISHED

}
