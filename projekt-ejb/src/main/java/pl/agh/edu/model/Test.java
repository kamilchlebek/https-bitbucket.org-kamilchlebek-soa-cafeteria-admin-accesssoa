package pl.agh.edu.model;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;

@Stateless
public class Test {

    @RolesAllowed("staff")
    public void test(){
        System.out.println("Test");
    }
}
