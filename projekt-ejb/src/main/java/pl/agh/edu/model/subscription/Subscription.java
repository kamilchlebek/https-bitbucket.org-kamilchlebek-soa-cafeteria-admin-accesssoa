package pl.agh.edu.model.subscription;

import pl.agh.edu.model.User;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.util.*;

@Entity
@NamedQueries({
        @NamedQuery(
                name = "findByMenuItem",
                query = "select s from Subscription s, IN (s.menuItems) t where t.item.id = :id and s.deleted = false"
        )
})
public class Subscription {

    @Id
    @GeneratedValue
    private Integer id;

    @OneToMany(cascade = CascadeType.ALL)
    private List<SubscriptionItem> menuItems = new ArrayList<>();

    @OneToOne
    private User user;

    private String address;

    private Date date;

    private Boolean deleted = false;

    @ElementCollection
    @Enumerated(EnumType.STRING)
    private Set<DayOfWeek> days = new HashSet<>();

    public Subscription() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<SubscriptionItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<SubscriptionItem> menuItems) {
        this.menuItems = menuItems;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Set<DayOfWeek> getDays() {
        return days;
    }

    public void setDays(Set<DayOfWeek> days) {
        this.days = days;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "Subscription{" +
                "id=" + id +
                ", menuItems=" + menuItems +
                ", user=" + user +
                ", address='" + address + '\'' +
                ", date=" + date +
                ", days=" + days +
                '}';
    }
}
