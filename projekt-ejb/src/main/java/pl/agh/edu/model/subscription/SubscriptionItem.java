package pl.agh.edu.model.subscription;

import pl.agh.edu.model.order.OrderItem;

import javax.persistence.Entity;

@Entity
public class SubscriptionItem extends OrderItem {


    public static SubscriptionItem copyWithoutId(OrderItem orderItem){
        SubscriptionItem subscriptionItem = new SubscriptionItem();
        subscriptionItem.setQuantity(orderItem.getQuantity());
        subscriptionItem.setItem(orderItem.getItem());
        return subscriptionItem;
    }
}
