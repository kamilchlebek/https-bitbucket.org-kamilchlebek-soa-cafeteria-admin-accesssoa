package pl.agh.edu.model.order;

import pl.agh.edu.model.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "user_order")
@NamedQueries({
        @NamedQuery(
                name = "findOrdersByMenuItem",
                query = "select s from Order s, IN (s.menuItems) t where t.item.id = :id and s.deleted = false"
        )
})
public class Order implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;

    @OneToOne
    private User user;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<OrderItem> menuItems = new ArrayList<>();

    private Date date;

    private Boolean deleted = false;

    private String address;

    private String comment;

    @Enumerated
    private OrderStatus orderStatus = OrderStatus.PENDING;

    public Order() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<OrderItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<OrderItem> menuItems) {
        this.menuItems = menuItems;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
