package pl.agh.edu.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Menu {

    @Id
    @GeneratedValue
    private int id;

    private String name;

    @Enumerated(EnumType.STRING)
    private MenuState state = MenuState.PUBLISHED;

    @OneToMany(cascade = CascadeType.ALL,orphanRemoval = true)
    private List<MenuCategory> categories = new ArrayList<>();

    private Boolean deleted = false;

    public Menu() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MenuState getState() {
        return state;
    }

    public void setState(MenuState state) {
        this.state = state;
    }

    public List<MenuCategory> getCategories() {
        return categories;
    }

    public List<MenuCategory> getCategoriesFiltered() {
        return categories.stream().filter(x -> !x.getDeleted()).collect(Collectors.toList());
    }

    public void setCategories(List<MenuCategory> categories) {
        this.categories = categories;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "name='" + name + '\'' +
                ", state=" + state +
                ", categories=" + categories +
                '}';
    }

    public Menu copyForRest(){

        Menu menu = new Menu();
        menu.setId(this.id);
        menu.setName(this.name);
        menu.setState(this.state);
        menu.setCategories(this.categories);

        return menu;
    }


}
