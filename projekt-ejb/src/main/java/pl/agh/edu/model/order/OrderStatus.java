package pl.agh.edu.model.order;

public enum OrderStatus {
    PENDING, DONE, DELIVERED, BEING_DELIVERED, WAITING_FOR_PICKUP
}
