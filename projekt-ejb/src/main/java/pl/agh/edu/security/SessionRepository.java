package pl.agh.edu.security;


import javax.ejb.Singleton;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class SessionRepository {

    private Map<String,String> loggedUsers = new HashMap<>();

    // true -> already.xml
    public boolean isAlreadyLogged(String email, String sessionId) {

        if(email == null || email.length() == 0)
            return false;

        String savedSessionId = loggedUsers.get(email);

        if(savedSessionId == null){
            login(email,sessionId);
            return false;
        }
        else {
            return !savedSessionId.equals(sessionId);
        }

    }

    public void login(String email, String sessionId) {
        loggedUsers.put(email,sessionId);
    }

    public void logout(String email){
        loggedUsers.remove(email);
    }

}
