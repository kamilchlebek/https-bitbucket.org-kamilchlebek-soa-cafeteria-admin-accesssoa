package pl.agh.edu;

import pl.agh.edu.model.*;

import java.util.ArrayList;
import java.util.List;

public class SampleMenu {

    public static Menu get(){

        Ingredient ingridient = new Ingredient();
        ingridient.setName("ziemniaki");
        ingridient.setUnit(Unit.GRAM);
        ingridient.setValue(100.0);

        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingridient);

        Menu menu = new Menu();
        menu.setName("testowe");
        menu.setState(MenuState.PUBLISHED);

        List<MenuItem> items = new ArrayList<>();


        MenuCategory category = new MenuCategory();
        category.setName("kategoria");
        category.setItems(items);

        MenuItem menuItem = new MenuItem();
        menuItem.setName("Jakas rzecz");
        menuItem.setImgUrl("http://lorempixel.com/130/100/food/");
        menuItem.setCategory(category);
        menuItem.setIngredients(ingredients);
        items.add(menuItem);

        List<MenuCategory> categories = new ArrayList<>();
        categories.add(category);
        // ====================================




        // ====================================
        menu.setCategories(categories);

        return menu;
    }
}
