package pl.agh.edu.persistence;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

@Singleton
public class PersistenceManager {

    @PersistenceUnit(unitName = "primary")
    private EntityManagerFactory emf;

    private EntityManager em;

    @PostConstruct
    public void setUp(){
        em = emf.createEntityManager();
    }

    public EntityManager getEm() {
        return em;
    }
}
