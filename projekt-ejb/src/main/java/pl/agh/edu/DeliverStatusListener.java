package pl.agh.edu;

import org.jboss.logging.Logger;
import pl.agh.edu.dao.OrdersDao;
import pl.agh.edu.model.DeliverResponse;
import pl.agh.edu.model.order.Order;
import pl.agh.edu.model.order.OrderStatus;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

@MessageDriven(name = "DeliverStatusListener", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = " java:jboss/exported/queue/deliverStatus"),
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge") })
public class DeliverStatusListener implements MessageListener {

    private Logger LOG = Logger.getLogger(DeliverStatusListener.class);

    @Inject
    private OrdersDao ordersDao;

    public void onMessage(Message message) {

        try {
            ObjectMessage objectMessage = (ObjectMessage) message;
            DeliverResponse response = (DeliverResponse)objectMessage.getObject();

            Order order = ordersDao.findById(response.getId());
            order.setOrderStatus(OrderStatus.DELIVERED);
            ordersDao.update(order);

            LOG.info("Deliver status received");
        } catch (JMSException e) {
            LOG.error(e);
        }

    }

}