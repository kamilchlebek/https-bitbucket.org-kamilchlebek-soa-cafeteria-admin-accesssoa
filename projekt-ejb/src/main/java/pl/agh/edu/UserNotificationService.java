package pl.agh.edu;

import org.jboss.logging.Logger;
import pl.agh.edu.model.User;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class UserNotificationService {

    private Logger LOG = Logger.getLogger(UserNotificationService.class);

    public void notifyUser(User user, String message){

        LOG.info("Sending email to : " + user.getEmail() + ", message: " + message);

    }



}
