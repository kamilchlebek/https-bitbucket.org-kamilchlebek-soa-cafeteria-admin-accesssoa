package pl.agh.edu;

import pl.agh.edu.dao.MenusDao;
import pl.agh.edu.model.Menu;
import pl.agh.edu.model.MenuCategory;
import pl.agh.edu.model.MenuItem;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
@RolesAllowed("menu")
public class MenusCategoryService {

    @Inject
    private MenusDao menusDao;

    public void create(Menu menu) {
        menusDao.create(menu);
    }

    public void update(Menu menu) {
        menusDao.update(menu);
    }

    public void deleteMenu(Menu menu) {
        menusDao.deleteMenu(menu);
    }

    public void deleteCategory(MenuCategory category) {

    }

    public void deleteItem(MenuItem menuItem) {


    }
}
