package pl.agh.edu.dao;

import org.hibernate.Session;
import pl.agh.edu.MenuItemChangedEventGenerator;
import pl.agh.edu.model.Menu;
import pl.agh.edu.model.MenuCategory;
import pl.agh.edu.model.MenuItem;
import pl.agh.edu.model.MenuState;
import pl.agh.edu.persistence.PersistenceManager;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@ApplicationScoped
public class MenusDao {

    @Inject
    private PersistenceManager persistenceManager;

    @Inject
    private MenuItemChangedEventGenerator menuItemChangedEventGenerator;

    private EntityManager em;
    @PostConstruct
    public void init(){
        em = persistenceManager.getEm();
    }

    public List<Menu> getAllMenus(){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Menu> criteria = cb.createQuery(Menu.class);
        Root<Menu> menu = criteria.from(Menu.class);
        criteria.select(menu).where(cb.and(cb.equal(menu.get("state"), MenuState.PUBLISHED), cb.equal(menu.get("deleted"), false)));
        return em.createQuery(criteria).getResultList();
    }

    public void deleteMenu(Integer menuId){
        Menu menu = em.find(Menu.class, menuId);
        em.remove(menu);
    }


    public void create(Menu menu) {
        em.persist(menu);
    }

    public void update(Menu menu) {
        Session session = persistenceManager.getEm().unwrap(Session.class);
        em.merge(menu);
        session.flush();
    }

    public void deleteMenu(Menu menu) {
        em.remove(menu);
    }

    public Menu getById(Integer menuId) {
        return em.find(Menu.class,menuId);
    }

    public MenuItem getMenuItemById(Integer menuItemId) {

        try {
            Query query = em.createNamedQuery("findMenuItemById");
            query.setParameter("id", menuItemId);
            return (MenuItem) query.getSingleResult();
        }
        catch(Exception e){
            return null;
        }


    }

    public MenuCategory getCategoryById(Integer menuCategoryId) {
        Query query = em.createNamedQuery("findMenuCategoryById");
        query.setParameter("id", menuCategoryId);
        return (MenuCategory) query.getSingleResult();
    }

    public void saveMenuItem(MenuItem menuItem) {
        em.merge(menuItem);
    }

    public void delete(MenuCategory category) {
        em.remove(category);
    }

    public void delete(MenuItem menuItem) {
        em.remove(menuItem);
    }
}
