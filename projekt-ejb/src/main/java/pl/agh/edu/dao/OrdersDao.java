package pl.agh.edu.dao;

import pl.agh.edu.model.MenuItem;
import pl.agh.edu.model.order.Order;
import pl.agh.edu.model.order.OrderItem;
import pl.agh.edu.model.subscription.Subscription;
import pl.agh.edu.persistence.PersistenceManager;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import java.util.List;

@ApplicationScoped
public class OrdersDao {

    @Inject
    private PersistenceManager persistenceManager;

    public void update(Order order) {

        persistenceManager.getEm().merge(order);

    }


    @RolesAllowed({"admin", "staff"})
    public List<Order> getAll(){
        return persistenceManager.getEm().createQuery("from Order o where o.deleted = false", Order.class).getResultList();
    }

    @RolesAllowed({"customer"})
    public List<Order> findForUser(String email){
        EntityManager em = persistenceManager.getEm();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Order> criteria = cb.createQuery(Order.class);
        Root<Order> order = criteria.from(Order.class);
        criteria.select(order).where(cb.and(cb.equal(order.get("user").get("email"), email), cb.equal(order.get("deleted"), false)));
        return em.createQuery(criteria).getResultList();
    }

    @RolesAllowed({"admin","customer", "menu"})
    public void delete(Order order) {
        EntityManager em = persistenceManager.getEm();
        em.remove(order);
    }

    public List<MenuItem> findTopOrdered() {
        Query query =  persistenceManager.getEm().createNamedQuery("findTopOrderedItems",MenuItem.class);
        return query.getResultList();
    }

    public List<Order> findByMenuItem(MenuItem menuItem) {
        Query query =  persistenceManager.getEm().createNamedQuery("findOrdersByMenuItem",Order.class);
        query.setParameter("id", menuItem.getId());
        return query.getResultList();
    }

    public Order findById(Integer id) {
        return persistenceManager.getEm().find(Order.class,id);
    }

}
