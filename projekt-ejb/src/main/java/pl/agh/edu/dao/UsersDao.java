package pl.agh.edu.dao;

import pl.agh.edu.model.User;
import pl.agh.edu.persistence.PersistenceManager;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.Query;
import java.util.List;

import static javafx.scene.input.KeyCode.T;

@ApplicationScoped
public class UsersDao {

    @Inject
    private PersistenceManager persistenceManager;

    public List<User> getAllUsers(){
        return persistenceManager.getEm().createQuery("from User", User.class).getResultList();
    }

    public User findByEmail(String email){
        Query query= persistenceManager.getEm().createQuery("from User where email = :email");
        query.setParameter("email", email);
        return (User) query.getSingleResult();
    }


    public User findById(Integer id){
        return persistenceManager.getEm().find(User.class,id);
    }

    public void update(User user) {
        persistenceManager.getEm().merge(user);
    }

}
