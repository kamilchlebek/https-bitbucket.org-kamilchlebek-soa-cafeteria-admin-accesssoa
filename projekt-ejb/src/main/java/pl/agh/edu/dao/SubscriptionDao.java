package pl.agh.edu.dao;

import pl.agh.edu.model.MenuCategory;
import pl.agh.edu.model.MenuItem;
import pl.agh.edu.model.order.Order;
import pl.agh.edu.model.subscription.Subscription;
import pl.agh.edu.persistence.PersistenceManager;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@ApplicationScoped
public class SubscriptionDao {

    @Inject
    private PersistenceManager persistenceManager;

    public void update(Subscription subscription) {
        persistenceManager.getEm().merge(subscription);
    }

    @RolesAllowed({"admin", "staff"})
    public List<Subscription> getAll(){
        return persistenceManager.getEm().createQuery("from Subscription where deleted = false", Subscription.class).getResultList();
    }

    @RolesAllowed({"customer"})
    public List<Subscription> findForUser(String email){
        EntityManager em = persistenceManager.getEm();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Subscription> criteria = cb.createQuery(Subscription.class);
        Root<Subscription> subs = criteria.from(Subscription.class);
        criteria.select(subs).where(cb.and(cb.equal(subs.get("user").get("email"), email),cb.equal(subs.get("deleted"), false)));
        return em.createQuery(criteria).getResultList();
    }

    @RolesAllowed({"admin", "customer", "menu"})
    public void delete(Subscription subscription) {
        persistenceManager.getEm().remove(subscription);
    }

    public List<Subscription> findByMenuItem(MenuItem menuItem) {
        Query query =  persistenceManager.getEm().createNamedQuery("findByMenuItem",Subscription.class);
        query.setParameter("id", menuItem.getId());
        return query.getResultList();
    }
}
