package pl.agh.edu;

import org.jboss.logging.Logger;
import pl.agh.edu.model.DeliverRequest;
import pl.agh.edu.model.order.Order;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.*;

@Stateless
public class DeliverService {

    private Logger logger = Logger.getLogger(DeliverService.class);

    @Resource(mappedName = "java:/ConnectionFactory")
    private ConnectionFactory cf;

    @Resource(mappedName = "java:jboss/exported/queue/toDeliver")
    private Queue queueExample;

    private Connection connection;

    public void toDeliver(Order order) {
        try {
            connection = cf.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer publisher = session.createProducer(queueExample);
            connection.start();
            publisher.send(session.createObjectMessage(createMessage(order)));
        }
        catch (Exception exc) {
            logger.error("Błąd! "+exc);
            exc.printStackTrace();
        }
        finally {
            if (connection != null) {
                try {
                    connection.close();
                    connection = null;
                } catch (JMSException e) { logger.error(e); }
            }
        }
    }

    private DeliverRequest createMessage(Order order) {
        return new DeliverRequest(order.getId(),order.getUser().getEmail(),order.getAddress());
    }

}
