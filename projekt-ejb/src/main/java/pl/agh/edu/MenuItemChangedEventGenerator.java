package pl.agh.edu;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

@Stateless
public class MenuItemChangedEventGenerator {

    @Inject
    private Event<MenuItemChangedEvent> eventQueue;

    @Resource
    private SessionContext sctx;

    public void generateEvent() {
        sctx.getBusinessObject(MenuItemChangedEventGenerator.class).fireEvent(new MenuItemChangedEvent());
    }

    @Asynchronous
    public void fireEvent(final MenuItemChangedEvent event) {
        eventQueue.fire(event);
    }
}
