package pl.agh.edu;

import pl.agh.edu.dao.OrdersDao;
import pl.agh.edu.dao.SubscriptionDao;
import pl.agh.edu.dao.UsersDao;
import pl.agh.edu.exceptions.NeedAdminAccessException;
import pl.agh.edu.model.MenuItem;
import pl.agh.edu.model.order.Order;
import pl.agh.edu.model.subscription.Subscription;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.security.Principal;
import java.time.*;
import java.util.Date;
import java.util.List;

@Stateless
@RolesAllowed({"admin", "customer", "staff", "menu"})
public class SubscriptionService {

    @Resource
    private SessionContext ctx;

    @Inject
    private SubscriptionDao subscriptionDao;

    @Inject
    private UsersDao usersDao;

    @Inject
    private OrdersDao ordersDao;

    @Inject
    private Principal userPrincipal;

    @Inject
    private UserNotificationService userNotificationService;




    public void delete(Subscription subscription) {

        Principal callerPrincipal = ctx.getCallerPrincipal();
        if (!ctx.isCallerInRole("admin") && !callerPrincipal.getName().equals(subscription.getUser().getEmail())) {
            throw new NeedAdminAccessException();
        }

        subscription.setDeleted(true);
        subscriptionDao.update(subscription);

    }
    @RolesAllowed({"customer"})
    public void update(Subscription subscription) {
        subscriptionDao.update(subscription);
    }

    public void handleItemRemove(MenuItem menuItem) {

        List<Subscription> byMenuItem = subscriptionDao.findByMenuItem(menuItem);
        byMenuItem.forEach(this::notifyAndDelete);

    }

    private void notifyAndDelete(Subscription subscription) {
        if(subscription.getUser() != null){
            userNotificationService.notifyUser(subscription.getUser(), "Subskrybcja anulowana anulowane, zamow jeszcze raz");
        }
        subscription.setDeleted(true);
        subscriptionDao.update(subscription);
    }

    @RolesAllowed({"staff"})
    public void addOrdersForToday() {

        DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();

        List<Subscription> all = subscriptionDao.getAll();

        all
            .stream()
            .filter(x -> x.getDays().contains(dayOfWeek))
            .forEach( x -> {
                Order order = new Order();
                order.setUser(x.getUser());
                order.setAddress(x.getAddress());
                order.getMenuItems().addAll(x.getMenuItems());
                order.setDate(todayWithSourceTime(x.getDate()));

                ordersDao.update(order);
            });


    }

    private Date todayWithSourceTime(Date source){
        Instant instant = source.toInstant();

        LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());

        LocalDateTime now = LocalDateTime.now()
                .withHour(ldt.getHour())
                .withMinute(ldt.getMinute())
                .withSecond(ldt.getSecond());
        return Date.from(now.toInstant(ZoneOffset.of("+02:00")));

    }
}
