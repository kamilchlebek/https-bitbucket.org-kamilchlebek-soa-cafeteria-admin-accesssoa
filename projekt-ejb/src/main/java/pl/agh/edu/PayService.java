package pl.agh.edu;

import pl.agh.edu.model.order.Order;

import javax.ejb.Stateless;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.WebServiceRef;
import java.util.Date;
import java.util.GregorianCalendar;

@Stateless
public class PayService {

    @WebServiceRef(PaymentSubscriptionServiceService.class)
    private PaymentSubscriptionServiceService service;

    @WebServiceRef(SalaryPaymentServiceService.class)
    private SalaryPaymentServiceService salaryService;

    public void pay(Order order){

        if(service.getPaymentSubscriptionServicePort().isSubscribed(order.getUser().getEmail())){

            XMLGregorianCalendar date2 = makeDate();

            SalaryPayment salaryPayment = new SalaryPayment();
            salaryPayment.setUserEmail(order.getUser().getEmail());
            salaryPayment.setDate(date2);
            salaryPayment.setOrderId(order.getId());
            salaryPayment.setPrice(calculatePrice(order));

            salaryService.getSalaryPaymentServicePort().pay(salaryPayment);

        }

    }

    private XMLGregorianCalendar makeDate() {
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(new Date());
        XMLGregorianCalendar date2 = null;
        try {
            date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        return date2;
    }

    private Double calculatePrice(Order order) {

        return order
                .getMenuItems()
                .stream()
                .mapToDouble(x -> x.getItem().getPrice()*x.getQuantity())
                .sum();

    }


}
