package pl.agh.edu.exceptions;

public class NeedAdminAccessException extends RuntimeException {

    public NeedAdminAccessException() {
        super();
    }

    public NeedAdminAccessException(String message) {
        super(message);
    }

    public NeedAdminAccessException(String message, Throwable cause) {
        super(message, cause);
    }

    public NeedAdminAccessException(Throwable cause) {
        super(cause);
    }

    protected NeedAdminAccessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
