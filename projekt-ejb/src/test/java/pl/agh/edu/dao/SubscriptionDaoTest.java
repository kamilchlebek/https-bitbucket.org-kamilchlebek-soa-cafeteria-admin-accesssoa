package pl.agh.edu.dao;

import org.junit.Test;
import pl.agh.edu.model.subscription.Subscription;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class SubscriptionDaoTest {

    @Test
    public void test(){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("primary");
        EntityManager em = emf.createEntityManager();

        Query query = em.createNamedQuery("findByMenuItem");
        query.setParameter("id", 90);
        List<Subscription> s = query.getResultList();

        System.out.println(s);
    }

}