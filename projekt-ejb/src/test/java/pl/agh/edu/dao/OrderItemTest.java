package pl.agh.edu.dao;

import org.junit.Test;
import pl.agh.edu.model.MenuItem;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class OrderItemTest {

    @Test
    public void test(){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("primary");
        EntityManager em = emf.createEntityManager();

        Query query = em.createNamedQuery("findTopOrderedItems");
        List<MenuItem> s = query.getResultList();

        System.out.println(s);
    }

}