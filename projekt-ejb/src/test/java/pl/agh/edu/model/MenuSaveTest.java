package pl.agh.edu.model;

import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

public class MenuSaveTest {

    @Test
    public void test(){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("primary");
        EntityManager em = emf.createEntityManager();

        EntityTransaction tx = em.getTransaction();
        tx.begin();

        Ingredient ingridient = new Ingredient();
        ingridient.setName("ziemniaki");
        ingridient.setUnit(Unit.GRAM);
        ingridient.setValue(100.0);

        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingridient);

        Menu menu = new Menu();
        menu.setName("testowe");
        menu.setState(MenuState.PUBLISHED);

        List<MenuItem> items = new ArrayList<>();


        MenuCategory category = new MenuCategory();
        category.setName("kategoria");
        category.setItems(items);
        category.setMenu(menu);

        MenuItem menuItem = new MenuItem();
        menuItem.setName("jakies menu");
        menuItem.setImgUrl("http://lorempixel.com/130/100/food/");
        menuItem.setCategory(category);
        menuItem.setIngredients(ingredients);
        items.add(menuItem);

        List<MenuCategory> categories = new ArrayList<>();
        categories.add(category);
        // ====================================




        // ====================================
        menu.setCategories(categories);


        em.merge(menu);
        tx.commit();


        em.close();
    }

}