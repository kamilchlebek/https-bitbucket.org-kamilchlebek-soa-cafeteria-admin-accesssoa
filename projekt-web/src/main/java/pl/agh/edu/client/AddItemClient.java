package pl.agh.edu.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class AddItemClient {

    private static final Integer CATEGORY_ID = 272;

    public static void main(String[] args) {

        try {

            URL url = new URL("http://localhost:8080/rest/menu/category/" + CATEGORY_ID + "/add");

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            String input = "{\n" +
                    "  \"name\": \"jeszcze nowsza nowa potrawa!\",\n" +
                    "  \"ingredients\": [\n" +
                    "    {\n" +
                    "      \"name\": \"nowa pozycja!\",\n" +
                    "      \"unit\": \"GRAM\",\n" +
                    "      \"value\": 13.0\n" +
                    "    }\n" +
                    "  ],\n" +
                    "  \"imgUrl\": \"test\"\n" +
                    "}\n";

            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("Odpowiedź .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }

            conn.disconnect();

        } catch (IOException e) {

            e.printStackTrace();

        }

    }

}