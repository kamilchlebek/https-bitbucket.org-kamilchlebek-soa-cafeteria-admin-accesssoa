package pl.agh.edu;

import pl.agh.edu.dao.OrdersDao;
import pl.agh.edu.model.MenuItem;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import java.util.List;

@ManagedBean
@ApplicationScoped
public class TopItems {

    @Inject
    private OrdersDao ordersDao;

    private List<MenuItem> top;

    @Inject
    MenuItemChangedEventGenerator generator;

    @PostConstruct
    public void init(){
        update();
    }

    public void update(){
        top = ordersDao.findTopOrdered();
    }

    public List<MenuItem> getTop() {
        return top;
    }

    public void setTop(List<MenuItem> top) {
        this.top = top;
    }

    public void fireEventTest(){
        generator.generateEvent();
    }
}
