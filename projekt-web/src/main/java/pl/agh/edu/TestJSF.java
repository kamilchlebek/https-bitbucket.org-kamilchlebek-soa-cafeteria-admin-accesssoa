package pl.agh.edu;


import pl.agh.edu.model.Test;

import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

@ManagedBean
public class TestJSF {

    @Inject
    private Test test;

    public void test(){
        test.test();
    }
}
