package pl.agh.edu.security;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/app/logout")
public class LogoutServlet extends HttpServlet {

    @Inject
    private SessionRepository sessionRepository;

    private static final String LOGOUT = "/";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        sessionRepository.logout(request.getRemoteUser());

        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
        response.setStatus(401) ;
        request.getSession().invalidate();
        request.logout();
        response.sendRedirect(LOGOUT);

    }
}
