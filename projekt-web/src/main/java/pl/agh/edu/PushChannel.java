package pl.agh.edu;

import org.jboss.logging.Logger;
import org.primefaces.push.annotation.OnMessage;
import org.primefaces.push.annotation.PushEndpoint;
import org.primefaces.push.impl.JSONEncoder;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

@ManagedBean
@ViewScoped
@PushEndpoint("/pushChannel")
public class PushChannel implements Serializable {

    Logger logger = Logger.getLogger(PushChannel.class);

    @OnMessage(encoders = {JSONEncoder.class})
    public FacesMessage onMessage(FacesMessage message) {
        logger.info("PushResource.onMessage facesMessage");
        return message;
    }

}

