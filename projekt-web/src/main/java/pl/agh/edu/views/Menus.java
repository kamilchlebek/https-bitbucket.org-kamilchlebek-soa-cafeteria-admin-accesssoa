package pl.agh.edu.views;

import pl.agh.edu.MenuItemChangedEventGenerator;
import pl.agh.edu.MenusService;
import pl.agh.edu.dao.MenusDao;
import pl.agh.edu.model.Menu;
import pl.agh.edu.model.MenuCategory;
import pl.agh.edu.model.MenuItem;

import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import java.util.List;

@ManagedBean
public class Menus {

    @Inject
    private MenusDao menusDao;

    @Inject
    private MenusService menusService;

    @Inject
    private MenuItemChangedEventGenerator menuItemChangedEventGenerator;

    private Integer id;


    public List<Menu> menusList(){
        return menusDao.getAllMenus();
    }

    public void deleteMenu(Menu menu) {
        menusService.deleteMenu(menu);
    }

    public void deleteCategory(MenuCategory category) {
        menusService.deleteCategory(category);
    }

    public void deleteItem(MenuItem menuItem) {
        menusService.deleteItem(menuItem);
        menuItemChangedEventGenerator.generateEvent();

    }
}
