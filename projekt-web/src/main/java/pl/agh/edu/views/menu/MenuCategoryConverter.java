package pl.agh.edu.views.menu;

import pl.agh.edu.dao.MenusDao;
import pl.agh.edu.model.MenuCategory;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

@Named
public class MenuCategoryConverter  implements Converter {

    @Inject
    private MenusDao menuDao;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return menuDao.getCategoryById(Integer.valueOf(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return String.valueOf(((MenuCategory) value).getId());
    }

}

