package pl.agh.edu.views;

import pl.agh.edu.PaymentSubscriptionServiceService;
import pl.agh.edu.SalaryPayment;
import pl.agh.edu.SalaryPaymentServiceService;
import pl.agh.edu.dao.OrdersDao;
import pl.agh.edu.dao.UsersDao;
import pl.agh.edu.model.User;
import pl.agh.edu.model.order.Order;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.xml.ws.WebServiceRef;
import java.security.Principal;
import java.util.List;

@ManagedBean
public class PaymentOptions {

    @Inject
    private UsersDao userDao;

    @Inject
    private OrdersDao ordersDao;

    @Inject
    private Principal userPrincipal;

    private List<SalaryPayment> salaryPayments;

    private User user;

    private Order order = new Order();

    @WebServiceRef(PaymentSubscriptionServiceService.class)
    private PaymentSubscriptionServiceService service;

    @WebServiceRef(SalaryPaymentServiceService.class)
    private SalaryPaymentServiceService salarService;


private boolean preview;
    private boolean subscribed;
    @PostConstruct
    public void init(){

        user = userDao.findByEmail(userPrincipal.getName());
        subscribed = service.getPaymentSubscriptionServicePort().isSubscribed(user.getEmail());
        salaryPayments = salarService.getSalaryPaymentServicePort().getPay(user.getEmail());
        preview = false;
    }

    public User getUser() {
        return user;
    }

    public void save(){

        if(subscribed){
            service.getPaymentSubscriptionServicePort().subscribe(user.getEmail());
        } else {
            service.getPaymentSubscriptionServicePort().unSubscribe(user.getEmail());
        }

    }

    public List<SalaryPayment> getSalaryPayments() {
        return salaryPayments;
    }

    public void setSalaryPayments(List<SalaryPayment> salaryPayments) {
        this.salaryPayments = salaryPayments;
    }

    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    public void previewOrder(Integer orderId){
        order = ordersDao.findById(orderId);
        preview = true;

    }

    public void setUser(User user) {
        this.user = user;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public boolean isPreview() {
        return preview;
    }

    public void setPreview(boolean preview) {
        this.preview = preview;
    }
}
