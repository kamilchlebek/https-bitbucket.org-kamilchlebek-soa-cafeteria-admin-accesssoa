package pl.agh.edu.views.menu;

import org.primefaces.event.FlowEvent;
import pl.agh.edu.MenusService;
import pl.agh.edu.dao.MenusDao;
import pl.agh.edu.model.*;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.util.Map;

@ManagedBean
@ViewScoped
public class UpsertElementWizard {

    private Integer menuId;
    private Integer menuItemId;
    private Menu menu;
    private MenuItem menuItem;
    private MenuCategory sourceCategory;

    @Inject
    private MenusDao menusDao;

    @Inject
    private MenusService menusService;

    private boolean added;

    @PostConstruct
    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        menuId = Integer.valueOf(params.get("menuId"));
        menuItemId = params.get("menuItemId") == null ? null : Integer.valueOf(params.get("menuItemId"));
        updateMenu();
        added = false;
    }

    private void updateMenu() {
        menu = menusDao.getById(menuId);
        menuItem = menuItemId == null ? new MenuItem() : menusDao.getMenuItemById(menuItemId);
        sourceCategory = menuItem.getCategory();
    }

    public MenuItem getMenuItem() {
        return menuItem;
    }

    public Menu getMenu() {
        return menu;
    }

    public void save() {

        menusService.saveMenuItem(menuItem, sourceCategory);
        added = true;

    }

    public String onFlowProcess(FlowEvent event) {
        return event.getNewStep();
    }

    public boolean isAdded() {
        return added;
    }

    public Unit[] getUnits() {
        return Unit.values();
    }

    public void extend() {
        menuItem.getIngredients().add(new Ingredient());
    }

    public void remove(Ingredient ingredient) {
        menuItem.getIngredients().remove(ingredient);
    }

    public javax.el.MethodExpression dummyListener() {
        return null;
    }
}
