package pl.agh.edu.views.order;

import pl.agh.edu.OrderService;
import pl.agh.edu.dao.OrdersDao;
import pl.agh.edu.model.order.Order;
import pl.agh.edu.util.WebResources;

import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import java.security.Principal;
import java.util.List;

@ManagedBean
public class ManageAllOrders {

    @Inject
    private Principal principal;

    @Inject
    private OrdersDao ordersDao;

    @Inject
    private OrderService orderService;


    @Inject
    private ManageOrder manageOrder;


    public void deleteOrder(Order order){
        orderService.delete(order);
    }

    public String edit(Order order){
        manageOrder.setOrder(order);
        return "/app/index.xhtml";
    }

    public List<Order> getOrders(){
        if(WebResources.isUserInRole("admin") || WebResources.isUserInRole("staff")){
            return ordersDao.getAll();
        }
        else {
            return ordersDao.findForUser(principal.getName());
        }
    }

    public void makeComment(Order order){
        orderService.makeComment(order);
    }

    public void makeDish(Order order){
        orderService.makeDish(order);
    }

    public void toDeliver(Order order){
        orderService.toDeliver(order);
    }

    public void confirmPickedUp(Order order){
        orderService.confirmPickedUp(order);

    }
}
