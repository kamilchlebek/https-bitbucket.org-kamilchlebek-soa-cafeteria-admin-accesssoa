package pl.agh.edu.views.subscription;


import pl.agh.edu.SubscriptionService;

import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

@ManagedBean
public class TodaySubscriptions {

    @Inject
    private SubscriptionService subscriptionService;

    public String load(){
        subscriptionService.addOrdersForToday();
        return "/app/order/manage.xhtml?faces-redirect=true";
    }
}
