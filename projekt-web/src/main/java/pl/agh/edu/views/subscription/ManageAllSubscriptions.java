package pl.agh.edu.views.subscription;

import pl.agh.edu.SubscriptionService;
import pl.agh.edu.dao.SubscriptionDao;
import pl.agh.edu.model.subscription.Subscription;
import pl.agh.edu.util.WebResources;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.io.Serializable;
import java.security.Principal;
import java.time.DayOfWeek;
import java.util.List;

@ManagedBean
@ViewScoped
public class ManageAllSubscriptions implements Serializable {

    @Inject
    private Principal principal;

    @Inject
    private SubscriptionDao subscriptionDao;

    @Inject
    private SubscriptionService subscriptionService;

    private Subscription subscription = new Subscription();
    private boolean edit;


    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }


    public void edit(Subscription subscription) {
        this.subscription = subscription;
        edit = true;
    }

    public void save() {
        subscriptionService.update(subscription);
        subscription = new Subscription();
        edit = false;
    }

    public DayOfWeek[] days(){
        return DayOfWeek.values();
    }


    public List<Subscription> getSubscriptions(){
        if(WebResources.isUserInRole("admin")){
            return subscriptionDao.getAll();
        }
        else {
            return subscriptionDao.findForUser(principal.getName());
        }
    }

    public void deleteSubscription(Subscription subscription){
        subscriptionService.delete(subscription);
    }

}
