package pl.agh.edu.views.menu;

import pl.agh.edu.MenusService;
import pl.agh.edu.dao.MenusDao;
import pl.agh.edu.model.Menu;
import pl.agh.edu.model.MenuState;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

@ManagedBean
@ViewScoped
public class ManageMenus implements Serializable {

    private List<Menu> list;
    private Menu menu = new Menu();
    private boolean edit;

    @Inject
    private MenusDao menusDao;

    @Inject
    private MenusService menusService;

    @PostConstruct
    public void init() {
        updateMenusList();
    }

    public MenuState[] getStatuses() {
        return MenuState.values();
    }


    private void updateMenusList() {
        list = menusDao.getAllMenus();
    }

    public void add() {
        menusService.create(menu);
        updateMenusList();
        menu = new Menu();
    }

    public void edit(Menu menu) {
        this.menu = menu;
        edit = true;
    }

    public void save() {
        menusService.update(menu);
        menu = new Menu();
        edit = false;
    }

    public void delete(Menu menu) {
        menusService.deleteMenu(menu);
        updateMenusList();
    }

    public List<Menu> getList() {
        return list;
    }

    public Menu getMenu() {
        return menu;
    }

    public boolean isEdit() {
        return edit;
    }

}
