package pl.agh.edu.views.order;


import pl.agh.edu.OrderService;
import pl.agh.edu.model.MenuItem;
import pl.agh.edu.model.order.Order;
import pl.agh.edu.model.order.OrderItem;
import pl.agh.edu.model.subscription.Subscription;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.time.DayOfWeek;

@Named
@ConversationScoped
public class ManageOrder implements Serializable {

    @Inject
    private Conversation conversation;

    @SuppressWarnings("CdiUnproxyableBeanTypesInspection")
    @Inject
    private OrderService orderService;

    private Order order;
    private Subscription subscription;

    private Boolean hasSubscription;
    private Boolean edit;

    @PostConstruct
    public void init() {
        conversation.begin();
        order = new Order();
        edit = order.getId() != null && order.getId() != 0;
        subscription = new Subscription();
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Conversation getConversation() {
        return conversation;
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    public Boolean getHasSubscription() {
        return hasSubscription;
    }

    public void setHasSubscription(Boolean hasSubscription) {
        this.hasSubscription = hasSubscription;
    }

    public Boolean getEdit() {
        return edit;
    }

    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    public String endConversation() {
        orderService.update(order,hasSubscription,subscription);
        conversation.end();
        return "/app/order/manage.xhtml?faces-redirect=true";
    }

    public void addItem(MenuItem menuItem){

        boolean found = false;
        for (OrderItem orderItem : order.getMenuItems()) {
            if(orderItem.getItem().equals(menuItem)){
                orderItem.setQuantity(orderItem.getQuantity()+1);
                found = true;
            }
        }

        if(!found){
            OrderItem e = new OrderItem();
            e.setQuantity(1);
            e.setItem(menuItem);
            order.getMenuItems().add(e);
        }

    }


    public void deleteItem(OrderItem orderItem){
        order.getMenuItems().remove(orderItem);
    }

    public String summary(){
        return "/app/order/summary";
    }


    public DayOfWeek[] days(){
        return DayOfWeek.values();
    }
}
