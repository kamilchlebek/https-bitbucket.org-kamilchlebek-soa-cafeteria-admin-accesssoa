package pl.agh.edu.views.menu;

import pl.agh.edu.MenusService;
import pl.agh.edu.dao.MenusDao;
import pl.agh.edu.model.Menu;
import pl.agh.edu.model.MenuItem;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.io.Serializable;

@ManagedBean
@ViewScoped
public class ManageElements implements Serializable {

    private Integer menuId;
    private Menu menu;

    @Inject
    private MenusDao menusDao;

    @Inject
    private MenusService menusService;

    @PostConstruct
    public void init() {
        menuId = Integer.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("menuId"));
        updateMenu();
    }

    private void updateMenu() {
        menu = menusDao.getById(menuId);
    }

    public Menu getMenu() {
        return menu;
    }


    public void delete(MenuItem menuItem) {
        menuItem.getCategory().getItems().remove(menuItem);
        menusService.update(menu);
    }


}
