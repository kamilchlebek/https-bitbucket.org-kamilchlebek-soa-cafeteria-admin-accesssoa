package pl.agh.edu.views.menu;

import pl.agh.edu.MenusService;
import pl.agh.edu.dao.MenusDao;
import pl.agh.edu.model.Menu;
import pl.agh.edu.model.MenuCategory;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.io.Serializable;

@ManagedBean
@ViewScoped
public class ManageSingleMenu implements Serializable {

    private Integer menuId;

    private MenuCategory category = new MenuCategory();
    private boolean edit;

    private Menu menu;

    @Inject
    private MenusDao menusDao;

    @Inject
    private MenusService menusService;

    @PostConstruct
    public void init() {
        menuId = Integer.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("menuId"));
        updateMenu();
    }

    private void updateMenu() {
        menu = menusDao.getById(menuId);
    }

    public Menu getMenu() {
        return menu;
    }

    public void add() {
        menu.getCategories().add(category);
        category.setMenu(menu);
        menusService.update(menu);
        updateMenu();
        category = new MenuCategory();
    }

    public void edit(MenuCategory menu) {
        this.category = menu;
        edit = true;
    }

    public void save() {
        menusService.update(menu);
        category = new MenuCategory();
        edit = false;
    }

    public void delete(MenuCategory category) {
        menu.getCategories().remove(category);
        menusService.update(menu);
        updateMenu();
    }

    public MenuCategory getCategory() {
        return category;
    }

    public boolean isEdit() {
        return edit;
    }

}
