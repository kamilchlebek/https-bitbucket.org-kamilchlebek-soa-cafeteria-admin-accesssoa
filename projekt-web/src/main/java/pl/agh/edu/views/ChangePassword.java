package pl.agh.edu.views;

import pl.agh.edu.ChangePasswordService;
import pl.agh.edu.dao.UsersDao;
import pl.agh.edu.model.User;
import pl.agh.edu.util.WebResources;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.security.Principal;
import java.util.Collections;
import java.util.List;

@ManagedBean
public class ChangePassword {

    @Inject
    private ChangePasswordService changePasswordService;

    @Inject
    private UsersDao userDao;

    @Inject
    private Principal userPrincipal;

    private Integer userId;
    private String password;

    public ChangePassword() {
    }


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String changePassword(){
        changePasswordService.changePassword(userId,password);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Zmieniono hasło", "ok"));
        return null;
    }

    public List<User> getUsers(){
        if(WebResources.isUserInRole("admin")){
            return userDao.getAllUsers();
        }
        else {
            return Collections.singletonList(userDao.findByEmail(userPrincipal.getName()));
        }
    }

}
