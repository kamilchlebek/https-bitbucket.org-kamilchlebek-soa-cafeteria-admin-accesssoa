package pl.agh.edu.filters;


import pl.agh.edu.security.SessionRepository;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "TestFilter", urlPatterns = {"/app/*"})
public class OneSessionFilter implements Filter {

    @Inject
    private SessionRepository sessionRepository;

    @Override
    public void init(FilterConfig cfg) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {


        HttpServletRequest request = ((HttpServletRequest) req);
        HttpServletResponse response = (HttpServletResponse) resp;

        if(sessionRepository.isAlreadyLogged(request.getRemoteUser(),request.getSession().getId())){
            request.getSession().invalidate();
            request.logout();
            response.sendRedirect("/already.xhtml");
            return;
        }
        chain.doFilter(req,resp);


    }

    @Override
    public void destroy() {
    }
}


