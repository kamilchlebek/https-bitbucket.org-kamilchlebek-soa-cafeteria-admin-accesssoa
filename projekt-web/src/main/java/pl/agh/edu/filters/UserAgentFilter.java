package pl.agh.edu.filters;


import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "UserAgentFilter", urlPatterns = {"/app/*"})
public class UserAgentFilter implements Filter {

    private static final String[] SUPPORTED_USER_AGENT = { "Chrome" };
    private static final String KEY_BAD_BROWSER_URL = "/filtered.xhtml";

    @Override
    public void init(FilterConfig cfg) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {

        String userAgent = ((HttpServletRequest) req).getHeader("User-Agent");
        for (String browser_id : SUPPORTED_USER_AGENT) {
            if (userAgent.contains(browser_id)){
                chain.doFilter(req, resp);
                return;
            }
        }
        ((HttpServletResponse) resp).sendRedirect(KEY_BAD_BROWSER_URL);
    }

    @Override
    public void destroy() {
    }
}


