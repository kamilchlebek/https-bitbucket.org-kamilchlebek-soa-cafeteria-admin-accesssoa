package pl.agh.edu.rest;

import pl.agh.edu.dao.MenusDao;
import pl.agh.edu.model.Menu;
import pl.agh.edu.model.MenuCategory;
import pl.agh.edu.model.MenuItem;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.List;
import java.util.Locale;

@Path("/menu")
@RequestScoped
@ManagedBean
public class MenuREST {

    @Inject
    private MenusDao menusDao;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = "admin")
    public List<Menu> loadMenu() {
        List<Menu> allMenus = menusDao.getAllMenus();
        if (allMenus == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return allMenus;
    }

    @GET
    @Path("/{id:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response loadMenu(@PathParam("id") Integer id, @Context Request request) {

        Variant.VariantListBuilder vb = Variant.VariantListBuilder.newInstance();
        vb.languages(new Locale("en"), new Locale("es")).add();

        List<Variant> variants = vb.build();

        Variant v = request.selectVariant(variants);

        Menu entity = menusDao.getById(id);
        if (entity == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        entity = entity.copyForRest();

        entity.setName(entity.getName() + " w języku: " + v.getLanguageString());
        Response.ResponseBuilder builder = Response.ok(entity);
        builder.type(v.getMediaType())
                .language(v.getLanguage())
                .header("Content-Encoding", v.getEncoding());

        return builder.build();

    }

    @GET
    @Path("/category/{cat:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public MenuCategory loadCategory(@PathParam("cat") Integer categoryId) {
        MenuCategory categoryById = menusDao.getCategoryById(categoryId);
        if (categoryById == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return categoryById;
    }

    @POST
    @Path("/category/{id:[0-9][0-9]*}/add")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addItemToCategory(@PathParam("id") Integer categoryId, MenuItem menuItem) {

        MenuCategory categoryById = menusDao.getCategoryById(categoryId);
        categoryById.getItems().add(menuItem);

        return Response.ok().build();
    }


}
