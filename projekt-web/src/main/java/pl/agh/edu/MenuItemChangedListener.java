package pl.agh.edu;

import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;

import javax.enterprise.event.Observes;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

@ManagedBean
public class MenuItemChangedListener {

    @Inject
    private TopItems topItems;

    public void consumeEvent(@Observes MenuItemChangedEvent myEvent) throws InterruptedException {
        topItems.update();
        EventBus eventBus = EventBusFactory.getDefault().eventBus();
        eventBus.publish("/pushChannel", "{\"status\":\"refresh\"}");
        System.out.println("PUBLISHED! ");
    }

}
